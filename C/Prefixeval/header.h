#ifndef header
#define header

struct Stack
{
    int stack[10];
    int top;
};
struct Stack st;

int pop();
void push(int);
int getType(char);

#endif