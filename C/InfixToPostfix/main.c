/*
 **Infix To postfix conversion
 *
 */ 

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "prototype.h"

int main()
{
    printf("\n Enter any infix expression : ");
    scanf("%s",infix);
    fflush(stdin);
    strcpy(postfix, "");
    infixtoPostfix(infix, postfix);
    printf("\n The corresponding postfix expression is : %s\n",postfix);
    return 0;
}
