#include <stdio.h>
#include <stdlib.h>
#include "operation.h"
void menu(){
    printf("\n ***** MENU ***** \n");
    printf("1. to push element on stack \n");
    printf("2. to pop element form stack\n");
    printf("3. to display elements of stack\n");
    printf("4. to exit \n");
    int choice;
    printf("Enter choice: ");
    scanf("%d",&choice);

    switch (choice)
    {
        case 1:{
            int num;
            printf("Enter the number to be pushed to stack: ");
            scanf("%d",&num);
            push(num);
        }    
        break;
        case 2:{
            int popped;
            popped = pop();
            printf("Popped element is %d \n",popped);
        }
        break;
        case 3:
            display();
        break;
        case 4:
            exit(0);
        break;
        default:
            printf("Invalid choice!!!!\n");
        break;
    }   
}