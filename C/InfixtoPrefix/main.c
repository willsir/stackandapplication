/**
 * Infix to Prefix conversion
 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "prototype.h"

int main()
{
    printf("\n Enter any infix expression : ");
    scanf("%s",infix);
    fflush(stdin);
    reverse(infix);
    strcpy(postfix, "");
    infixtoPostfix(temp, postfix);
    printf("\n The corresponding postfix expression is : ");
    printf("%s\n",postfix);
    strcpy(temp,"");
    reverse(postfix);
    printf("\n The prefix expression is : %s\n",temp);
    return 0;
}
