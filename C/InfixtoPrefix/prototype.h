#ifndef PROTOTYPE
#define PROTOTYPE

#define size 100

    struct Stack
    {
        int stack[size];
        int top;
    }st;
    void reverse(char[]);
    void push(char );
    char pop();
    int getPriority(char);
    void infixtoPostfix(char [], char []);

    char infix[100], postfix[100], temp[100];

#endif