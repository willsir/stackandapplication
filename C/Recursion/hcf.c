#include <stdio.h>
int HCF(int, int);
int main()
{
    int num1, num2, res;
    printf("\n Enter the two numbers: ");
    scanf("%d %d", &num1, &num2);
    res = HCF(num1, num2);
    printf("\n GCD of %d and %d = %d\n", num1, num2, res);
    return 0;
}
int HCF(int x, int y)
{
    int rem;
    rem = x%y;
    if(rem==0)
        return y;
    else        
        return (HCF(y, rem));
}
