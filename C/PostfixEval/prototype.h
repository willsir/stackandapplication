#ifndef PROTOTYPE
#define PROTOTYPE

#define size 100

    struct Stack
    {
        float stack[size];
        int top;
    }st;
    
    void push(float );
    float pop();
    float evalPostfixExp(char[]);

#endif