/**
 * Postfix Evaluation
 */

#include <stdio.h>
#define size 100
#include "prototype.h"

int main()
{
    st.top = -1;
    float value;
    char exp[size];
    printf("\n Enter any postfix expression : ");
    scanf("%s",exp);
    fflush(stdin);
    value = evalPostfixExp(exp);
    printf("\n Value of the postfix expression = %.2f\n", value);
    return 0;
}
