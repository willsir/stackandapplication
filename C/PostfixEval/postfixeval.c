#include <ctype.h>
#include "prototype.h"

float evalPostfixExp(char exp[])
{
    int i=0;
    float op1, op2, value;
    while(exp[i] != '\0')
    {
        if(isdigit(exp[i]))                     //if digit is encounter pushing to stack
            push((exp[i]));
        
        else{
            op2 = pop() - '0';                        //top element
            op1 = pop() - '0';                        //top-1 element
            switch(exp[i]){
                case '+':
                    value = op1 + op2;
                break;
                case '-':
                    value = op1 - op2;
                break;
                case '/':
                    value = op1 / op2;
                break;
                case '*':
                    value = op1 * op2;
                break;
                case '%':
                    value = (int)op1 % (int)op2;    //casting is necessary inorder to perform modulus
                break;
            }
            push(value);
        }
        i++;
    }
    return(pop());
}
