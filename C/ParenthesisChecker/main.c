/*
 *********************************************
 * Application of Stack to check parenthesis *
 *********************************************
*/
#include <stdio.h>
#include <string.h>
#define size 100
#include "prototype.h"

int main()
{   
    st.top = -1;
    int i,flag = 1;
    char expression[size],temp;
    printf("Enter the expression: ");
    scanf("%s",expression);
    
    for(i=0; i<strlen(expression);i++){
        if(expression[i] == '(' || expression[i] == '{' || expression[i] == '['){
            push(expression[i]);
        }
        if(expression[i] == ')' || expression[i] == '}' || expression[i] == ']'){
            if(st.top == -1){                            //null case ie no left bracket are present
                flag = 0;
            }
            else{
                temp = pop();
                if(expression[i] == ')' && (temp == '{' || temp == '['))
                    flag = 0;
                if(expression[i] == '}' && (temp == '(' || temp == '['))
                    flag = 0;
                if(expression[i] == ']' && (temp == '{' || temp == '('))
                    flag = 0;
            }
        }
    }
    if(st.top >= 0)              //incase left brackets > right brackets
        flag = 0;
        
    if(flag == 1){
        printf("The expression is correct!!!\n");
    }
    else{
        printf("The expression is incorrect!!!\n");
    }
    return 0;
}
