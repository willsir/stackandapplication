/*****************************************
 * Application of Stack to reverse list  *
 *****************************************
*/
#include <stdio.h>
#define size 100
#include "prototype.h"
int main(int argc, char const *argv[])
{
    st.top = -1;   
    int n,i,popped;
    int list[size];
    printf("Enter the elements to be inserted in list: ");
    scanf("%d",&n);
    for(i=0; i<n;i++){
        printf("index[%d]: ",i);
        scanf("%d",&list[i]);
    }

    printf("***Adding inserted element to the stack***\n");
    for(i=0; i<n; i++){
        push(list[i]);
    }

    printf("***Array in reverse order***\n");
    for(i=0;i<n;i++){
        list[i] = pop();
    }

    for(i=0; i<n; i++){
        printf("index[%d] = %d\n",i,list[i]);
    }
    return 0;
}




